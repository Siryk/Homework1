﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerProgram
{
    class WorkerEqualityComparer : IEqualityComparer<WorkerBase>
    {
        public bool Equals(WorkerBase workerOne, WorkerBase workerTwo)
        {
            if (Object.ReferenceEquals(workerOne, workerTwo)) return true;
            return (workerOne?.Name.Equals(workerTwo?.Name) ?? false) && (workerOne?.Salary.Equals(workerTwo?.Salary) ?? false);
        }

        public int GetHashCode(WorkerBase obj)
        {
            int hashTaskName = obj.Name == null ? 0 : obj.Name.GetHashCode();
            int hashTaskXml = obj.Salary == null ? 0 : obj.Salary.GetHashCode();
            return hashTaskName ^ hashTaskXml;
        }
    }
}
