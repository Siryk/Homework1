﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerProgram
{
    sealed class WorkerDay : WorkerBase
    {
        public decimal BaseRate { get; set; }

        public WorkerDay(Guid id, string name, decimal baseRate) : base(id, name)
        {
            BaseRate = baseRate;
            Calculate();
        }

        public override void Calculate()
        {
            Salary = BaseRate;
        }
    }
}
