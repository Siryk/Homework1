﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerProgram
{
    sealed class WorkerTime : WorkerBase
    {
        public decimal HourSalary { get; set; }
        public int CountHours { get; set; }

        public WorkerTime(Guid id, string name, int countHour, decimal hourSalary) : base(id, name)
        {
            CountHours = countHour;
            HourSalary = hourSalary;
            Calculate();
        }

        public override void Calculate()
        {
            Salary = HourSalary * CountHours;
        }
    }
}
