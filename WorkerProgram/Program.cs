﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WorkerProgram
{
    //public class IncorectMenuValue : Exception
    //{
    //   public IncorectMenuValue(string message) : base(message)
    //   {

    //   }

    //}
    

    static class Menu
    {

        private static readonly Dictionary<int, string> _menuDictionary = new Dictionary<int, string>()
        {
            {1,"Add Based On Time Worker\n" },
            {2,"Add Fixed Worker\n"},
            {3,"Show Workers\n"},
            {4,"Show (N) Worker\n"},
            {5,"Exit\n"}
        };

        public static int Show()
        {

            int choice = 0;

            while (true)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.SetCursorPosition(45, 0);
                Console.WriteLine("Worker Base");
                Console.ResetColor();
                Console.WriteLine();
                Console.Write(
                    $"Please Enter Your Choice:\n{_menuDictionary.Aggregate(new StringBuilder(), (item, stringMenu) => item.Append(stringMenu.Key).Append(")").Append(stringMenu.Value), (menu) => menu.ToString())}");
                Console.WriteLine("------------------------");
                Console.Write("Enter number:");

                try

                {
                    Int32.TryParse(Console.ReadLine(),out choice);

                    if (choice > _menuDictionary.FirstOrDefault().Key && choice <= _menuDictionary.Count)
                    {
                        break;
                    }
                    else
                    {
                        throw new Exception("Menu item with this number does not exist.");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + "To continue, press any button...");
                }
                Console.ReadKey();
            }
            return choice;
        }
    }

    class Program
    {
        private static readonly List<WorkerBase> WorkerCollection = new List<WorkerBase>();

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            while (true)
            {
                WorkerBase worker;
                Console.Clear();

                switch (Menu.Show())
                {
                    case 1:
                        worker = AddWorkers(1);
                        if (worker != null)
                        {
                            if (!WorkerCollection.Contains(worker, new WorkerEqualityComparer()))
                            {
                                WorkerCollection.Add(worker);
                                WorkerCollection.Sort();
                            }
                            else
                            {
                                Console.WriteLine("This worker contains in base.");
                            }
                        }
                        break;
                    case 2:
                        worker = AddWorkers(2);
                        if (worker != null)
                        {
                            if (!WorkerCollection.Contains(worker, new WorkerEqualityComparer()))
                            {
                                WorkerCollection.Add(worker);
                                WorkerCollection.Sort();
                            }
                            else
                            {
                                Console.WriteLine("This worker contains in base.");
                            }
                        }
                        break;
                    case 3:
                        if (!WorkerCollection.Count.Equals(0))
                        {
                            foreach (var work in WorkerCollection)
                            {
                                Console.WriteLine(work);
                            }
                        }
                        else
                        {
                            Console.WriteLine("You don`t add worker. Please add someone and try more...");
                        }
                        Console.ReadKey();
                        break;
                    case 4:
                        Console.Write("Enter N:");
                        var number = Validation.GetValidInt();
                        if (!WorkerCollection.Count.Equals(0))
                        {
                            for (int i = 0; i < number; i++)
                            {
                                Console.WriteLine(WorkerCollection[i]);
                            }
                        }
                        else
                        {
                            Console.WriteLine("You don`t add worker. Please add someone and try more...");
                        }
                        Console.ReadKey();
                        break;
                    case 5:
                        return;
                }
            }
        }

        private static WorkerBase AddWorkers(int type)
        {
            string name = "";
            decimal salary = 0;

            switch (type)
            {
                case 1:
                    Console.Write("Name:");
                    name = Validation.GetValidString();
                    Console.Write("Count:");
                    var count = Validation.GetValidInt();
                    Console.Write("Time salary:");
                    salary = Validation.GetValidDecimal();
                    return new WorkerTime(Guid.NewGuid(), name ,count, salary);
                case 2:
                    Console.Write("Name:");
                    name = Validation.GetValidString();
                    Console.Write("Salary:");
                    salary = Validation.GetValidDecimal();
                    return new WorkerDay(Guid.NewGuid(), name, salary);
            }
            return null;
        }
    }
}
