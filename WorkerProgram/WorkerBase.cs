﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerProgram
{
    abstract class WorkerBase : IComparable<WorkerBase>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }

        protected WorkerBase(Guid id, string name, decimal salary = 0)
        {
            Id = id;
            Name = name;
            Salary = salary;
        }

        public abstract void Calculate();

        public int CompareTo(WorkerBase other)
        {
            int result = -Salary.CompareTo(other.Salary);

            if (result != 0)
            {
                return result;
            }

            result = String.Compare(Name, other.Name, StringComparison.Ordinal);

            if (result != 0)
            {
                return result;
            }

            return 0;
        }

        public override string ToString()
        {
            return $"ID: {Id} | NAME: {Name} | SALARY: {Salary.ToString("c3", new CultureInfo("en-US"))}";
        }
    }
}
