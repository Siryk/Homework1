﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace WorkerProgram
{
    static class Validation
    {
        public static string GetValidString()
        {
            while (true)
            {
                var result = Console.ReadLine();

                if (!string.IsNullOrEmpty(result))
                {
                    if (new Regex(@"^[A-Z][a-zA-Z\s`]+$").IsMatch(result))
                    {
                        return result;
                    }
                    else
                    {
                        Console.Clear();
                        Console.Write(
                            "Your string has invalid characters. Please try enter valid string. (Example:John Malkovich)\nNew value:");
                    }
                }
                else
                {
                    Console.Clear();
                    Console.Write(
                        "Your string has empty. Please try enter valid string. (Example:John Malkovich)\nNew value:");
                }
            }
        }

        public static int GetValidInt()
        {
            while (true)
            {
                if (Int32.TryParse(Console.ReadLine(), out int result))
                {
                    return result;
                }
                else
                {
                    Console.Clear();
                    Console.Write("Your integer number has invalid value. Please try enter valid int. (Example:5)\nNew value:");
                }
            }
        }

        public static decimal GetValidDecimal()
        {
            while (true)
            {
                if (Decimal.TryParse(Console.ReadLine(), out decimal result))
                {
                    return result;
                }
                else
                {
                    Console.Clear();
                    Console.Write("Your decimal has invalid value. Please try enter valid decimal number. (Example:3740,40)\nNew value:");
                }
            }
        }
    }
}
