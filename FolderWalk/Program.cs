﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FolderWalk
{
    class Program
    {

        static class Menu
        {
            //public enum MenuItems
            //{
            //    [Description("Add folders\n")]
            //    Added = 1,
            //    [Description("Delete folders\n")]
            //    Delete = 2,
            //    [Description("Show folders\n")]
            //    Show = 3,
            //    [Description("Exit\n")]
            //    Exit = 4
            //}

            private static readonly Dictionary<int, string> _menuDictionary = new Dictionary<int, string>()
            {
                {1, "Add folders\n"},
                {2, "Delete folders\n"},
                {3, "Show folders\n"},
                {4, "Exit\n"},
            };

            public static int Show()
            {
                int choice = 0;

                while (true)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.SetCursorPosition(45, 0);
                    Console.WriteLine("Multi Folders");
                    Console.ResetColor();
                    Console.WriteLine();
                    Console.Write(
                        $"Please Enter Your Choice:\n{_menuDictionary.Aggregate(new StringBuilder(), (item, stringMenu) => item.Append(stringMenu.Key).Append(")").Append(stringMenu.Value), (menu) => menu.ToString())}");
                    Console.WriteLine("------------------------");
                    Console.Write("Enter number:");

                    try

                    {
                        Int32.TryParse(Console.ReadLine(), out choice);

                        if (choice >= _menuDictionary.FirstOrDefault().Key && choice <= _menuDictionary.Count)
                        {
                            break;
                        }
                        else
                        {
                            throw new Exception("Menu item with this number does not exist.");
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message + "To continue, press any button...");
                    }
                    Console.ReadKey();
                }
                return choice;
            }
        }
         
        private const int MaxValue = 10;

        static void Main()
        {

            while (true)
            {
                Console.Clear();

                string path;
                int countFolders;
                int validInt;

                switch (Menu.Show())
                {
                    case 1:
                        countFolders = Folder.CountFolders();
                        Console.Write("Enter N-folder:");
                        validInt = Validation.GetValidInt();

                        if ((countFolders + validInt) > MaxValue)
                        {
                            Console.WriteLine("The entered number of levels exceeds the allowable amount.");
                            Console.ReadKey();
                            break;
                        }
                        else
                        {
                            try
                            {
                                path = Folder.CreatePath(countFolders, Folder.AssemblyDirectory);
                                Folder.CreateFolders(countFolders, path, countFolders + validInt);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine($"ERROR:{e.Message}");
                            }
                            
                        }
                        Console.ReadKey();
                        break;
                    case 2:
                        countFolders = Folder.CountFolders();
                        Console.Write("Enter N-folder:");
                        validInt = Validation.GetValidInt();

                        if ((countFolders - validInt) < 0)
                        {
                            Console.WriteLine("The entered number of levels does not exist.");
                            Console.ReadKey();
                            break;
                        }
                        else
                        {
                            try
                            {
                                path = Folder.CreatePath(countFolders, Folder.AssemblyDirectory);
                                Folder.DeleteFolders(validInt, path);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine($"ERROR:{e.Message}");
                            }
                        }
                        Console.ReadKey();
                        break;
                    case 3:
                        Folder.ShowAllFoldersUnder(Folder.AssemblyDirectory);
                        Console.ReadKey();
                        break;
                    case 4:
                        return;
                }
            }
        }
    }
}
