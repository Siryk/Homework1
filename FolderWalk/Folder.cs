﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderWalk
{
    public static class Folder
    {
        public static string AssemblyDirectory => Directory.GetCurrentDirectory();

        public static void ShowAllFoldersUnder(string path, int indent = 0)
        {
            try
            {

                if ((File.GetAttributes(path) & FileAttributes.ReparsePoint)
                    != FileAttributes.ReparsePoint)
                {
                    foreach (string file in Directory.GetFiles(path, "*", SearchOption.TopDirectoryOnly))
                    {
                        Console.WriteLine("{0}{1}", new string(' ', indent) + "|-> ", Path.GetFileName(file));
                    }

                    foreach (string folder in Directory.GetDirectories(path))
                    {
                        Console.WriteLine(
                            "{0}{1}", new string(' ', indent) + "|-> ", Path.GetFileName(folder));

                        ShowAllFoldersUnder(folder, indent + 2);
                    }
                }
            }
            catch (UnauthorizedAccessException) { }
        }

        public static int CountFolders(int i = 0)
        {
            return Directory.GetDirectories(Folder.AssemblyDirectory, $"level_{i}", SearchOption.AllDirectories).ToList().Count == 0 ? i : CountFolders(i + 1);
        }

        public static string CreatePath(int countFolders, string path)
        {
            for (int i = 0; i < countFolders; i++)
            {
                path = Path.Combine(path, "level_" + i);
            }
            return path;
        }

        public static void CreateFolders(int depth, string path, int count)
        {
            if (depth == count) return;
            var dir = Path.Combine(path, "level_" + depth);

            try
            {
                Directory.CreateDirectory(dir);
                Console.WriteLine($"Create folder:{dir}");
                File.Create(dir + ".txt").Close();
                Console.WriteLine($"Create file:{dir}.txt");
                CreateFolders(depth + 1, dir, count);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error:{e}");
                throw;
            }
        }

        public static void DeleteFolders(int i, string path)
        {
            if (i == 0)
            {
                return;
            }
            try
            {
                if (File.Exists(path + ".txt"))
                {
                    File.Delete(path + ".txt");
                    Console.WriteLine($"Delete file:{path}.txt");
                }
                Directory.Delete(path);
                Console.WriteLine($"Delete folder:{path}");
                DeleteFolders(i - 1, path.Remove(path.LastIndexOf('\\')));
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error:{e}");
                throw;
            }
        }
    }
}

