﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderWalk
{
    static class Validation
    {
        public static int GetValidInt()
        {
            while (true)
            {
                if (Int32.TryParse(Console.ReadLine(), out int result))
                {
                    return result;
                }
                else
                {
                    Console.Clear();
                    Console.Write(
                        "Your integer number has invalid value. Please try enter valid int. (Example:5)\nNew value:");
                }
            }
        }
    }
} 
